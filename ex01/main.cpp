/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/13 20:32:44 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/15 21:04:23 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include <iostream>

struct Data {
	std::string s1;
	int n;
	std::string s2;
};

void	*serialize(void) {

	int				n;	
	char			*d, *d2;
	unsigned int	i, i2;
	std::string		s1, s2;
	static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

	std::srand(time(0));
	n = std::rand();

	for (i = 0; i < 8; ++i) {
		s1.push_back(alphanum[std::rand() % (sizeof(alphanum) - 1)]);
		s2.push_back(alphanum[std::rand() % (sizeof(alphanum) - 1)]);
	}

	std::cout << "Data serialisation:" << std::endl;
	std::cout << s1 << std::endl;
	std::cout << n << std::endl;
	std::cout << s2 << std::endl;

	d = new char[sizeof(Data)];

	d2 = reinterpret_cast<char *>(&s1);
	for (i = i2 = 0; i < sizeof(s1); i++, i2++)
		d[i2] = d2[i];

	d2 = reinterpret_cast<char *>(&n);
	for (i = 0; i < sizeof(n); i++, i2++)
		d[i2] = d2[i];

	d2 = reinterpret_cast<char *>(&s2);
	for (i = 0; i < sizeof(s2); i++, i2++)
		d[i2] = d2[i];

	return static_cast<void *>(d);
}

Data	*deserialize(void *raw) {

	Data	*d = new Data;
	char	*cast = static_cast<char*>(raw);

	d->s1 = *(reinterpret_cast<std::string*>(cast));
	d->n = *(reinterpret_cast<int*>(cast + sizeof(d->s1)));
	d->s2 = *(reinterpret_cast<std::string*>(cast + sizeof(d->s1) + sizeof(d->n)));

	return d;
}

int		main(void) {

	void *a = serialize();
	Data *d = deserialize(a);

	std::cout << std::endl << "Data deserialisation:" << std::endl;
	std::cout << d->s1 << std::endl;
	std::cout << d->n << std::endl;
	std::cout << d->s2 << std::endl;

	return 0;
}
