/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/13 19:34:34 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/15 11:28:09 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

void	printChar(int i) {

	char	c;
	c = static_cast<char>(i);

	if (i < 0 || i > 127)
		std::cout << "char: impossible" << std::endl;
	else if (i < 33 || i == 127) {
		std::cout << "char: '" << "\\" << std::oct << static_cast<int>(c) << "'" << std::endl;
	}
	else
		std::cout << "char: '" << c << "'" << std::endl;
}

void	printFloat(float f, int i) {

	if (f == i)
		std::cout << "float: " << f << ".0f" << std::endl;
	else
		std::cout << "float: " << f << "f" << std::endl;
}

void	printDouble(double d, int i) {

	if (d == i)
		std::cout << "double: " << d << ".0" << std::endl;
	else
		std::cout << "double: " << d << std::endl;
}
