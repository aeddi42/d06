/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/13 13:49:35 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/15 11:30:10 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <cstdlib>

void	printChar(int i);
void	printFloat(float f, int i);
void	printDouble(double d, int i);

void	charConvert(int i) {

	float	f;
	double	d;

	f = static_cast<float>(i);
	d = static_cast<double>(i);

	printChar(i);
	std::cout << "int: " << std::dec << i << std::endl;
	printFloat(f, i);
	printDouble(d, i);
}

void	intConvert(int i) {

	float	f;
	double	d;

	f = static_cast<float>(i);
	d = static_cast<double>(i);

	printChar(i);
	std::cout << "int: " << std::dec << i << std::endl;
	printFloat(f, i);
	printDouble(d, i);
}

void	floatConvert(float f) {

	int		i;
	double	d;

	i = static_cast<int>(f);
	d = static_cast<double>(f);

	printChar(i);
	std::cout << "int: " << std::dec << i << std::endl;
	printFloat(f, i);
	printDouble(d, i);
}

void	doubleConvert(double d) {

	int		i;
	float	f;

	i = static_cast<int>(d);
	f = static_cast<float>(d);

	printChar(i);
	std::cout << "int: " << std::dec << i << std::endl;
	printFloat(f, i);
	printDouble(d, i);
}

void	printOther(std::string literal) {

	if (literal == "+inf" || literal == "+inff") {
		std::cout << "char: impossible" << std::endl;
		std::cout << "int: impossible" << std::endl;
		std::cout << "float: +inff" << std::endl;
		std::cout << "double: +inf" << std::endl;
	}
	else if (literal == "-inf" || literal == "-inff") {
		std::cout << "char: impossible" << std::endl;
		std::cout << "int: impossible" << std::endl;
		std::cout << "float: -inff" << std::endl;
		std::cout << "double: -inf" << std::endl;
	}
	else if (literal == "nanf" || literal == "nan") {
		std::cout << "char: impossible" << std::endl;
		std::cout << "int: impossible" << std::endl;
		std::cout << "float: nanf" << std::endl;
		std::cout << "double: nan" << std::endl;
	}
	else {
		std::cout << "char: impossible" << std::endl;
		std::cout << "int: impossible" << std::endl;
		std::cout << "float: impossible" << std::endl;
		std::cout << "double: impossible" << std::endl;
	}

	return;
}

void	check_type(std::string literal) {

	char cb1, cb2, ce1;

	cb1 = *literal.begin();
	cb2 = *(literal.begin() + 1);
	ce1 = *literal.rbegin();

	if (literal == "0") {
		charConvert(0);
	}
	else if	(((cb1 == '-' || cb1 == '+') && std::isdigit(cb2)) || std::isdigit(cb1)) {
		if (ce1 == 'f' && literal.find('.') != std::string::npos)
			floatConvert(std::strtof(literal.c_str(), NULL));
		else if (std::isalnum(ce1) && literal.find('.') != std::string::npos)
			doubleConvert(std::strtod(literal.c_str(), NULL));
		else if (std::strtol(literal.c_str(), NULL, 10))
			intConvert(static_cast<int>(std::strtol(literal.c_str(), NULL, 10)));
		else
			printOther(literal);
	}
	else if (std::isalpha(cb1) && literal.length() == 1)
		charConvert(static_cast<int>(cb1));
	else if (cb1 == '\\' && ((std::strtol((literal.c_str() + 1), NULL, 10) > 0 && (std::strtol((literal.c_str() + 1), NULL, 10) < 128))|| cb2 == '0'))
		charConvert(std::strtol((literal.c_str() + 1), NULL, 10));
	else
			printOther(literal);

}

int		main(int ac, char **av) {

	if (ac > 2)
		std::cerr << "Too much arguments ! (max 1)" << std::endl;
	else if (ac < 2)
		std::cerr << "No arguments ! You must add one to using this program." << std::endl;
	else {
		std::string	literal(av[1]);
		check_type(literal);
	}

	return 0;
}
