/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plastic </var/spool/mail/plastic>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/15 14:06:37 by plastic           #+#    #+#             */
/*   Updated: 2015/04/15 14:36:08 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ctime>
#include <cstdlib>
#include <iostream>

class Base { public: virtual ~Base(void) {} };

class A : public Base {};
class B : public Base {};
class C : public Base {};

Base*	generate(void) {

	static int	seed = 0;
	std::srand(time(NULL) + seed++);

	if (std::rand() % 3 == 0) {
		std::cout << "Class A instanciated" << std::endl;
		return new A;
	}
	else if (std::rand() % 2 == 0) {
		std::cout << "Class B instanciated" << std::endl;
		return new B;
	}
	else {
		std::cout << "Class C instanciated" << std::endl;
		return new C;
	}
}

void	identify_from_pointer(Base* p) {

	if (dynamic_cast<A*>(p))
		std::cout << "Class A identified from pointer" << std::endl;
	else if (dynamic_cast<B*>(p))
		std::cout << "Class B identified from pointer" << std::endl;
	else if (dynamic_cast<C*>(p))
		std::cout << "Class C identified from pointer" << std::endl;
}

void	identify_from_reference(Base& p) {

	if (dynamic_cast<A*>(&p))
		std::cout << "Class A identified from reference" << std::endl;
	else if (dynamic_cast<B*>(&p))
		std::cout << "Class B identified from reference" << std::endl;
	else if (dynamic_cast<C*>(&p))
		std::cout << "Class C identified from reference" << std::endl;
}

int		main(void) {

	Base	*tmp;

	for (int i = 0; i < 5; i++) {
		tmp = generate();
		identify_from_pointer(tmp);
		identify_from_reference(*tmp);
		std::cout << std::endl;
		delete tmp;
	}

	return 0;
}
